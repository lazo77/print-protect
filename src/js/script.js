$(document).ready(function () {

//Плавная прокрутка до блока Клиенты
   $('.scroll_to').click(function () {
      var scroll_el = $(this).attr('href');
      var destination = $(scroll_el).offset().top;
      $('html, body').animate({scrollTop: destination}, 600);
      return false;
   });

//Нужная картинка для блоков без текста на стр. Продукты и Технологии
//   $('.img_div').each(function () {
//      var id_bg = $(this).attr('id');
//      var bg_Url = 'url\(img\/' + id_bg + '\.jpg\)';
//      $(this).css({'background': bg_Url, 'background-size': 'cover'});
//   });

//Выравнивание по центру блоков с текстом на стр. Продукты и Технологии
   var handleMatchMedia = function (mediaQuery) {
      if (mediaQuery.matches) {
         $('.text_div_right').parent().css('justify-content', 'flex-start');
         $('.text_div_left').parent().css('justify-content', 'flex-end');
      } else {
         $('.text_div').removeAttr("style");
      }
   };
   mql = window.matchMedia('all and (min-width: 781px)');

   handleMatchMedia(mql);
   mql.addListener(handleMatchMedia);

//Слайдер на главной
   $('#header_slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      dots: true,
      speed: 500,
      arrows: false
   });

//Слайдер стр. Карточка продукта
   $('#slider_bottom').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: false,
      speed: 500,
      arrows: true,
      responsive: [{

            breakpoint: 571,
            settings: {
               arrows: false
            }

         }
//         , {
//
//            breakpoint: 600,
//            settings: {
//               slidesToShow: 2,
//               dots: true
//            }
//
//         }
      ]
   });

//Слайдер стр. Карточка технологий
   $('#slider_top').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      initialSlide: 3,
      centerMode: true,
      variableWidth: true,
      dots: true,
      speed: 500,
      arrows: true,
      responsive: [{

            breakpoint: 1325,
            settings: {
               arrows: false
            }

         }
//         , {
//
//            breakpoint: 600,
//            settings: {
//               slidesToShow: 2,
//               dots: true
//            }
//
//         }
      ]
   });

//стр.Заказчикам спойлеры
   $(".custom_block_headline").on("click", function (){
      $(".custom_arrow", this).toggle();
      $(this).next().slideToggle();
   });

//Модальное окно
   $('.go_mod').click(function (event) {
      event.preventDefault();
      $('#overlay').fadeIn(300,
              function () {
                 $('#modal_form')
                         .css('display', 'block')
                         .animate({opacity: 1, top: '60%'}, 300);
              });
   });

   $('#modal_close, #overlay').click(function () {
      $('#modal_form')
              .animate({opacity: 0, top: '40%'}, 300,
                      function () {
                         $(this).css('display', 'none');
                         $('#overlay').fadeOut(300);
                      }
              );
   });


//Модальное окно с лицензиями
   $('.lic_item').click(function (event) {
      event.preventDefault();
      var id = $(this).attr('id');
      var imgUrl = 'url\(img\/' + id + '\.png\)';
//      console.log(imgUrl);
      $('#overlay').fadeIn(300,
              function () {
                 $('#mod_license')
                         .css({'display': 'block', 'background': imgUrl, 'background-size': '100% 100%'})
                         .animate({opacity: 1, top: '50%'}, 300);
              });
   });

   $('#overlay').click(function () {
      $('#mod_license')
              .animate({opacity: 0, top: '45%'}, 300,
                      function () {
                         $(this).removeAttr("style");
                         $('#overlay').fadeOut(300);
                      }
              );
   });
   $('.menu-tops').hover(function() {
		$('.fixed-shadow').stop(true, true).fadeIn();
	}, function() {
		$('.fixed-shadow').fadeOut();
	});

	$('.but-mobile').click(function() {
		$('.menu-tops').toggle();
	});

	$('.menu-tops>ul>li.sub-mm>a, .menu-tops>ul>li.sub-mm>span').click(function() {
		$(this).next().toggle();
		return false;
	});

	$('.menu-tops>ul>li>ul>li.sub-mm>a, .menu-tops>ul>li>ul>li.sub-mm>span').click(function() {
		$(this).next().toggle();
		return false;
	});
});
//   $('.lic_item').click(function() {
//        console.log($(this).attr('id'));
//    });